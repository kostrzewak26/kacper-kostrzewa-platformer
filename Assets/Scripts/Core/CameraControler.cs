﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Transform startPosition;
    private Vector3 currentPos;
    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        currentPos = startPosition.position;
    }
    private void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, currentPos , ref velocity, speed);
    }

    public void MoveToNewRoom(Transform _newRoom)
    {
        currentPos = _newRoom.position;
    }
}
