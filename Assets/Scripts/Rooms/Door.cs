﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private Transform previousRoom;
    [SerializeField] private Transform nextRoom;
    [SerializeField] private CameraControler cam;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (collision.transform.position.x < transform.position.x)
            {
                cam.MoveToNewRoom(nextRoom);
                nextRoom.GetComponentInParent<Room>().ActivateRoom(true);
                previousRoom.GetComponentInParent<Room>().ActivateRoom(false);
            }
                
            else
            {
                cam.MoveToNewRoom(previousRoom);
                previousRoom.GetComponentInParent<Room>().ActivateRoom(true);
                nextRoom.GetComponentInParent<Room>().ActivateRoom(false);
            }
               
        }
    }


}
